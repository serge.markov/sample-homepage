.. post:: Nov 22, 2017
   :tags: docfactor, sphinx
   :author: Serge Markov

Docfactor test
============

Эта статья написана для теста на матерклассе.

Первый абзац поста будет использоваться как краткое содержание в архивах и RSS.
О том, как настроить отображение, читайте в `Post Excerpts and Images
<https://ablog.readthedocs.org/manual/post-excerpts-and-images/>`_.

Ссылки на посты можно делать по имени файла: из разметки ``:ref:`first-post``` получается ссылка :ref:`first-post`.
Подробнее это описано в документе `Cross-Referencing Blog Pages
<https://ablog.readthedocs.org/manual/cross-referencing-blog-pages/>`_.
