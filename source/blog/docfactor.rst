.. post:: Nov 26, 2017
   :tags: docfactor, sphinx
   :author: Serge Markov

Docfactor publish
=================

Эта статья написана для теста на матерклассе.

`Документация по синтаксису <http://www.sphinx-doc.org/en/stable/rest.html>`_

Первый абзац поста будет использоваться как краткое содержание в архивах и RSS.
О том, как настроить отображение, читайте в `Post Excerpts and Images
<https://ablog.readthedocs.org/manual/post-excerpts-and-images/>`_.

Ссылки на посты можно делать по имени файла: из разметки ``:ref:`first-post``` получается ссылка :ref:`first-post`.
Подробнее это описано в документе `Cross-Referencing Blog Pages
<https://ablog.readthedocs.org/manual/cross-referencing-blog-pages/>`_.
